<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'riml-aesthetics' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ') =:Mz4aYlv3+;D>4j&<(&04zPH{xl74g[M;<ctor7I>oF:^e1I2FNJ?b/NpoLE^' );
define( 'SECURE_AUTH_KEY',  '0P^!UH-82zQE&RK ^Kk|v}W@!ApY# SG%oa2.;G`#HE_GjQG{>Vz$ux(r?!^RMrk' );
define( 'LOGGED_IN_KEY',    'Rah/48MYfpucRSH-Pf&$j!GKfkT6h#6`6Jt3[cCT,1U>+C40=3RrZ 53?G%{ #i&' );
define( 'NONCE_KEY',        'N(X~q=Y)u$PaL6s^A{,wbpCYfC#sW9xjvNR7Mo[UHdY tGl%-qb$ AucFh:253HH' );
define( 'AUTH_SALT',        'P].]jaUo~IjzTF0Cw2zWr.sND4u>r!Tw=1>Nkj[2$_`4(%KB-urWFU7jIIOLxBJo' );
define( 'SECURE_AUTH_SALT', 'L{C.vmYi5wufLC@:4N]R#`^oB5NObnq+U!VHw2oxnbV4JhI%]V,2Q!-(#}9Kip4d' );
define( 'LOGGED_IN_SALT',   'M|!xR9`AI_;zl8Uc{L4w5EYn6X_0^-^Glg}l|!{B6Gd@*kvR RPj7u<ELT)F*sc_' );
define( 'NONCE_SALT',       'pE6Izh7#Sdi|mRZ[;P27tU=ia>M|.gsZoUR0U+nHlF`e)12V}Bq3G;T8X$|c$AS~' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
