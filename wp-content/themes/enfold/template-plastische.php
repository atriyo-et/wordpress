<?php
/*
Template Name: Inside page
*/
?>

<?php get_header(); ?>

<section id="plastische-banner" class="plastische-banner"></section>

<section id="detail" class="section-detail">
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-4 mt-4 mb-5">
                <h2 class="mb-3 fs-4 mt-2 font-weight-bold text-left">
                    In guten Händen
                </h2>
                <text class="text-50 fs-6">
                    Vertrauen ist essenziell, um sich für einen Eingriff in die Hände eines Spezialisten zu begeben. Fachliche Kompetenz auf höchstem Niveau sowie menschliches Einfühlvermögen bilden dabei die Grundlage für dieses Vertrauen. Bei Dr. Riml finden Sie genau das und sind garantiert in guten Händen.
                </text>
            </div>
            <div class="col-lg-4 mt-4 mb-5">
                <h2 class="mb-3 fs-4 mt-2 font-weight-bold text-left">
                    Perfektion
                </h2>
                <text class="text-50 fs-6">
                    Niemand ist perfekt, aber ist nicht der Weg das Ziel? Sie wollen die beste Version von sich selbst sein? Sie schätzen Perfektion bis ins Detail? Dann sind Sie hier genau richtig.
                </text>
            </div>
            <div class="col-lg-4 mt-4 mb-5">
                <h2 class="mb-3 fs-4 mt-2 font-weight-bold text-left">
                    Ästhetik
                </h2>
                <text class="text-50 fs-6">
                    Schön sein, schön werden, schön bleiben? Es stört Sie eine Kleinigkeit an Ihrem Körper? Oder möchten Sie ein neues Leben beginnen? Vieles ist möglich – Dr. Riml berät Sie gerne.
                </text>
            </div>
        </div>
    </div>
</section>

<section id="plastic-surgery" class="plastic-surgery">
    <div class="container col-lg-6 pt-5">
        <div class="row justify-content-center text-center ">
            <h1 class="mt-3 fs-1 text-dark">
                Plastische Chirurgie
            </h1>
            <?php $wpb_all_query = new WP_Query(['post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 3, 'category_name' => 'Plastische Chirurgie']);

            if ($wpb_all_query->have_posts()) :
                while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); ?>
                    <p class="p-3">
                        <?php the_content(); ?>
                    </p>
                <?php endwhile; ?>
            <?php wp_reset_postdata();
            else : ?>
                <p class="p-3"><?php _e('Sorry, nothings here.'); ?></p>
            <?php endif; ?>
        </div>
        <hr>
    </div>
</section>

<section id="section-spectrum" class="section-spectrum">
    <div class="container col-lg-6 pt-5">
        <div class="row justify-content-center text-center ">
            <h1 class="mt-3 fs-1 text-dark">
                Übersicht Spektrum
            </h1>
            <hr>
            <p class="p-3">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
        </div>
        <div class="text-center">
            <p class="pb-4 line">
                <button class="btn border border-dark  btn-spectrum" type="button" data-bs-toggle="collapse" data-bs-target="#Button1" aria-expanded="false" aria-controls="collapseExample">
                    GESICHT
                </button>
                <button class="btn border border-dark  btn-spectrum" type="button" data-bs-toggle="collapse" data-bs-target="#Button2" aria-expanded="false" aria-controls="collapseExample">
                    BRUST
                </button>
                <button class="btn border border-dark  btn-spectrum" type="button" data-bs-toggle="collapse" data-bs-target="#Button3" aria-expanded="false" aria-controls="collapseExample">
                    KÖRPER
                </button>
                <button class="btn border border-dark btn-spectrum" type="button" data-bs-toggle="collapse" data-bs-target="#Button4" aria-expanded="false" aria-controls="collapseExample">
                    HAND
                </button>
            </p>
        </div>
        <div class="collapsable-content">
            <div class="collapse multi-collapse" id="Button1">
                <div class="col-12 d-flex">
                    <div class="row">
                        <div class="col-12 col-md-6 p-4">
                            <img src="<?= get_template_directory_uri() . '/images/layout/'; ?>Plastische-Chirurgie-Dr-Riml-Gesicht-2-klein.jpg" class="mx-auto d-block" alt="...">
                            <div class="mt-4 text-lg-start">
                                <button class="btn border border-dark" type="button">
                                    JETZT BUCHEN
                                </button>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="text-start  p-3">
                                <p class="fs-5">GESICHT</p>
                            </div>
                            <div class="accordion" id="myAccordion1">
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse1">Lidkorrektur</button>
                                    </h2>
                                    <div id="collapse1" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Hängelider oder Tränensäcke können mit einer Lidstraffung korrigiert werden. Die Operation kann in örtlicher Betäubung ambulant durchgeführt werden. Die Narben am Lid verheilen in der Regel innerhalb kurzer Zeit und werden mit der Zeit so gut wie unsichtbar.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingTwo">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse2">Nasenkorrektur</button>
                                    </h2>
                                    <div id="collapse2" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Ob Nasenhöcker, zu breite oder schiefe Nase oder hängende Nasenspitze: Die Nase als Zentrum des Gesichts ist von besonderer Bedeutung für die Schönheit. Eine Nasenkorrektur kann diese Makel beseitigen und häufig auch die Nasenatmung verbessern. Die Operation wird meist in Vollnarkose durchgeführt. Häufig können sichtbare Narben vermieden werden.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingThree">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse3">Ohrkorrektur</button>
                                    </h2>
                                    <div id="collapse3" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Abstehende Ohren, ausgerissene Piercings, Tunnels: Sie müssen Ihr Ohr unter dem Haar verstecken? Ein kleiner Eingriff in örtlicher Betäubung kann Ihr Ohr dauerhaft korrigieren.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFour">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse4">Facelift, Fadenlifting</button>
                                    </h2>
                                    <div id="collapse4" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Mit zunehmendem Alter erschlafft die Haut im Gesicht und am Hals. Mit einem Facelift wird die Problemregion gestrafft und die Zeit um viele Jahre zurückgestellt. Die Operation kann je nach Ausmaß ambulant oder stationär, in örtlicher Betäubung oder Vollnarkose durchgeführt werden. Als Alternative steht Ihnen der PDO Fadenlift zur Verfügung. Hier werden selbstauflösende Fäden unter die Haut eingezogen, die zu einem Straffungseffekt führen, oder sogar abgesunkene Gesichtspartien wie die Wange oder die Augenbraue wieder heben können. Ein Fadenlifting wird in örtlicher Betäubung ambulant durchgeführt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFive">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse5">Botox, Hyaluron</button>
                                    </h2>
                                    <div id="collapse5" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Häufig zeigen sich bereits ab 20 Jahren erste Falten im Gesicht. Botox und Hyaluronsäure sind Medikamente, mit denen man Falten ohne Operation mildern kann. Die Behandlung muss alle 6 bis 24 Monate wiederholt werden und wird in der Praxis ambulant durchgeführt. Dr. Riml verwendet ausschließlich Medikamente von höchster Qualität. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingSix">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse6">Lippen</button>
                                    </h2>
                                    <div id="collapse6" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Natürlich volle Lippen sind ein Schönheitsideal, nach dem sich viele Frauen sehnen. Dieser Wunsch kann mit einer Injektion von Hyaluron einfach und schnell Wirklichkeit werden.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingSeven">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse7">Laser</button>
                                    </h2>
                                    <div id="collapse7" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Mit unserem modernen Lasergerät mit Diodenlaser, IPL und Nd-YAG können wir Ihnen ein breites Spektrum an Behandlungen anbieten. So ist die schmerzarme, dauerhafte Haarentfernung, die Entfernung von Tattoos und unerwünschten Permanent Make-up-Pigmenten, die Behandlung von Pigmentstörungen, störenden Äderchen (Couperose, Teleangiektasien, Besenreisern) und die Hautverjüngung möglich.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingEight">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse8">Needling, Radiofrequenz, Carbon-Peeling</button>
                                    </h2>
                                    <div id="collapse8" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Mit unseren modernen Geräten können wir ein großes Spektrum an Möglichkeiten zur Hautverjüngung und Korrektur von störenden Hauterscheinungen (Aknenarben, Narben, Falten) bieten.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingNine">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse9">Wiederherstellung</button>
                                    </h2>
                                    <div id="collapse9" class="accordion-collapse collapse" data-bs-parent="#myAccordion1">
                                        <div class="card-body">
                                            <p>Der weiße Hautkrebs ist die am häufigsten auftretende Krebsart beim Menschen. Wird er operativ und fachkundig entfernt, ist eine Heilung praktisch garantiert. Der Erfolg der Wiederherstellung des Gesichts ist maßgebend von der Erfahrung des Arztes. Während an öffentlichen Krankenhäusern solche Operationen meist von Assistenzärzten durchgeführt werden, kann Dr. Riml auf die Erfahrung von über 1000 Operationen bei weißem Hautkrebs zurückblicken. Entscheiden Sie selbst!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collapse multi-collapse" id="Button2">
                <div class="col-12 d-flex">
                    <div class="row">
                        <div class="col-12 col-md-6 p-4">
                            <img src="<?= get_template_directory_uri() . '/images/layout/'; ?>Plastische-Chirurgie-Dr-Riml-Brust-1-klein.jpg" class="mx-auto d-block " alt="...">
                            <div class="mt-4 text-lg-start">
                                <button class="btn border border-dark" type="button">
                                    JETZT BUCHEN
                                </button>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="text-start p-3">
                                <p class="fs-5">BRUST</p>
                            </div>
                            <div class="accordion " id="myAccordion2">
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapse10">Brustvergrößerung</button>
                                    </h2>
                                    <div id="collapse10" class="accordion-collapse collapse" data-bs-parent="#myAccordion2">
                                        <div class="card-body">
                                            <p>Viele Frauen leiden unter ihrer zu kleinen oder fehlgeformten Brust. Auch nach dem Stillen oder nach Gewichtsabnahme kann Brustvolumen verloren gehen. Hier kann mit einer Brustvergrößerung dauerhaft geholfen werden. Hierbei werden Silikonimplantate über oder unter den Brustmuskel eingelegt. In der gleichen Operation kann die Brust, falls nötig, gestrafft werden. Die Größe und Form des Implantats werden vor der Operation ausführlich mit der Patientin besprochen. Dr. Riml verwendet ausschließlich Silikonimplantate von höchster Qualität. Die Operation wird in Vollnarkose durchgeführt. Alternativ oder als Ergänzung kann auch Eigenfett für die Brustvergrößerung verwendet werden.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingTwo">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo">Bruststraffung</button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#myAccordion2">
                                        <div class="card-body">
                                            <p>Mit zunehmendem Alter, nach dem Stillen oder nach Gewichtsverlust verliert die weibliche Brust ihre schöne jugendliche Form. Bei der Bruststraffung wird die Brustwarze angehoben, überschüssige Haut entfernt und die Brustdrüse gestrafft. Eine Bruststraffung kann auch mit der Einlage von Silikonimplantaten kombiniert werden. In den meisten Fällen werden narbensparende Techniken eingesetzt, um ein ästhetisch perfektes Ergebnis zu erreichen. Die Operation wird in Vollnarkose durchgeführt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingThree">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree">Brustverkleinerung</button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#myAccordion2">
                                        <div class="card-body">
                                            <p>Eine zu große Brust kann beim Sport hinderlich sein und quälende Schulter-, Nacken- und Rückenschmerzen verursachen. Zudem fühlen sich viele Frauen auf ihre große Brust reduziert und weniger attraktiv. Mit einer Brustverkleinerung kann der Patientin diese Last erleichtert werden und eine Brust nach ihren Vorstellungen neu geformt werden. Meist kann diese Operation narbensparend durchgeführt werden. Die Operation wird in Vollnarkose durchgeführt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFour">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFour">Männerbrustkorrektur</button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#myAccordion2">
                                        <div class="card-body">
                                            <p>Immer mehr Männer leiden unter einer Vergrößerung der Brust. Dabei kann entweder die Brustdrüse selbst vergrößert sein (Gynäkomastie), überschüssiges Fettgewebe eingelagert sein (Hypermastie), oder beides kombiniert auftreten. Dies kann mit kaum sichtbaren Narben dauerhaft korrigiert werden. Dabei wird überschüssiges Fettgewebe abgesaugt und eine vergrößerte Brustdrüse operativ entfernt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFive">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFive">Brustwiederherstellung</button>
                                    </h2>
                                    <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#myAccordion2">
                                        <div class="card-body">
                                            <p>Die Diagnose Brustkrebs ist ein schwerer Schlag im Leben einer Frau. Die Plastische Chirurgie kann mit der Wiederherstellung der Brust der Patientin die Entstellung der Brustamputation ersparen und so maßgeblich die Lebensqualität steigern. Dr. Riml hat als Leiter der plastisch-rekonstruktiven Brustchirurgie des Brustzentrums St. Gallen große Erfahrung in der Brustwiederherstellung erworben. Der Patientin kann das volle Spektrum der plastisch-rekonstruktiven Chirurgie, von Wiederherstellung der ganzen oder Teilen der Brust, mit körpereigenem Gewebe oder Implantaten, über Wiederherstellung der Brustwarze bis zur Angleichung der gesunden Brust, angeboten werden.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collapse multi-collapse" id="Button3">
                <div class="col-12 d-flex">
                    <div class="row">
                        <div class="col-12 col-md-6 p-4">
                            <img src="<?= get_template_directory_uri() . '/images/layout/'; ?>Plastische-Chirurgie-Dr-Riml-Koerper-1-klein.jpg" class="mx-auto d-block" alt="...">
                            <div class="mt-4 text-lg-start">
                                <button class="btn btn-sm border border-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    JETZT BUCHEN
                                </button>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="text-start  p-3">
                                <p class="fs-5">KÖRPER</p>
                            </div>
                            <div class="accordion" id="myAccordion3">
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseOne">Oberarmstraffung</button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Insbesondere nach starkem Gewichtsverlust, jedoch auch mit zunehmendem Alter, kann die Haut am Oberarm erschlaffen. Dies kann mit einem Oberarmlifting einfach und dauerhaft korrigiert werden. Die Operation wird meist in Vollnarkose durchgeführt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingTwo">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo">Oberschenkelstraffung</button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Erschlaffte Haut an den Oberschenkeln kann weder durch Sport noch durch Diäten beseitigt werden. Hier hilft jedoch eine Oberschenkelstraffung. Bei Bedarf kann diese auch mit einer Fettabsaugung kombiniert werden. Meist können die Narben so gelegt werden, dass sie unter Bade- oder Unterwäsche gut versteckt werden können. Die Operation wird in Vollnarkose durchgeführt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingThree">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree">Bauchdeckenstraffung</button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Auch bei normalgewichtigen Menschen können in der Bauchregion hartnäckige Fettpolster und überschüssige Haut stören. Insbesondere nach einer Schwangerschaft kann zudem die Bauchmuskulatur erschlafft sein. Dies kann mit einer Bauchstraffung dauerhaft und ästhetisch perfekt korrigiert werden. Die Operation wird in Vollnarkose durchgeführt.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFour">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFour">Fettabsaugung</button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Leiden Sie unter Fettpolstern, die weder durch Sport noch Diät schmelzen? Mit einer Fettabsaugung kann dies ohne großen Aufwand und mit nur kleinen Narben dauerhaft korrigiert werden. Die Operation kann in örtlicher Betäubung oder Vollnarkose durchgeführt werden.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFive">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFive">Narbenkorrektur</button>
                                    </h2>
                                    <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Leider können Narben nicht zum Verschwinden gebracht werden. Jedoch können Verwachsungen zur Tiefe hin gelöst werden, verbreiterte Narben verschmälert werden und eingezogene Narben mit Eigenfett unterfüttert werden. Somit kann eine Narbenkorrektur eine Narbe deutlich unauffälliger machen und in vielen Fällen den Narbenschmerz lindern. Die Operation kann meist in örtlicher Betäubung ambulant durchgeführt werden.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingSix">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSix">Genitalkorrektur</button>
                                    </h2>
                                    <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Meist handelt es sich um vergrößerte innere Schamlippen, die Frauen nicht nur optisch stören, sondern auch beim Sport oder Geschlechtsverkehr schmerzen. Mit einer kleinen Korrektur, die in örtlicher Betäubung oder Vollnarkose durchgeführt wird, kann den Patientinnen dauerhaft geholfen werden. Die Narben sind später zumeist so gut wie unsichtbar.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingSeven">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSeven">Übermäßiges Schwitzen</button>
                                    </h2>
                                    <div id="collapseSeven" class="accordion-collapse collapse" data-bs-parent="#myAccordion3">
                                        <div class="card-body">
                                            <p>Sie neigen zu starkem Schwitzen unter der Achsel, an den Händen oder Fußsohlen? Auch ohne Operation ist hier Hilfe möglich. Das Medikament Botox hemmt die Schweißproduktion für viele Monate</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collapse multi-collapse" id="Button4">
                <div class="col-12 d-flex">
                    <div class="row">
                        <div class="col-12 col-md-6 p-4">
                            <img src="<?= get_template_directory_uri() . '/images/layout/'; ?>Plastische-Chirurgie-Dr-Riml-Hand-1-klein.jpg" class="mx-auto d-block" alt="...">
                            <div class="mt-4 text-lg-start">
                                <button class="btn btn-sm border border-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    JETZT BUCHEN
                                </button>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="text-start p-3">
                                <p class="fs-5">HAND</p>
                            </div>
                            <div class="accordion" id="myAccordion4">
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseOne">Karpaltunnelsyndrom</button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#myAccordion4">
                                        <div class="card-body">
                                            <p>Das Karpaltunnelsyndrom ist eine äußerst häufige Erkrankung, bei der der Mittelnerv (Nervus medianus) im Handwurzelbereich eingeengt ist. Dies kann Kribbeln der Finger, Kraftverlust und Schmerzen verursachen. Schienen helfen hier zumeist nur kurzfristig. Mit einer kleinen Operation, welche ambulant in örtlicher Betäubung durchgeführt wird, kann den PatientInnen dauerhaft geholfen werden. Auch die anderen Nerven des Armes können Engstellen aufweisen (Loge de Guyon Syndrom, Sulcus Nervi Ulnaris Syndrom und andere), die chirurgisch gut behoben werden können.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingTwo">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo">Dupuytren Kontrakturen</button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#myAccordion4">
                                        <div class="card-body">
                                            <p>Dabei handelt es sich um eine Erkrankung, bei der sich das Bindegewebe in der Handfläche strangartig verdickt. Somit wird die Streckbarkeit der Finger zunehmend beeinträchtigt. Diese Stränge können entfernt werden, und somit die Streckbarkeit der Finger wiederhergestellt werden.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingThree">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree">Schnellender Finger</button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#myAccordion4">
                                        <div class="card-body">
                                            <p>Die Beugesehnen der Finger werden durch Ringbänder an den Knochen gehalten. Häufig wird die Sehne unter diesen Bändern eingeengt, was sich durch ein lästiges Schnellen der Finger bemerkbar macht. Eine risikoarme Operation in örtlicher Betäubung kann hier dauerhaft Abhilfe schaffen.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFour">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFour">Traumatologie der Hand</button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#myAccordion4">
                                        <div class="card-body">
                                            <p>Von der Schnittverletzung an der Hand, über Verletzungen der Gefäße, Nerven und Sehnen bis zu Knochenbrüchen an der Hand und komplexen Handverletzungen: Im Akutfall zählt rasche fachkundige Diagnostik und Therapie, um die Unfallfolgen möglichst kleinzuhalten.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingFive">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFive">Handgelenkschmerzen, Arthrose</button>
                                    </h2>
                                    <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#myAccordion4">
                                        <div class="card-body">
                                            <p>Schmerzen an der Hand oder den Fingern können viele Ursachen haben. Es ist wichtig, die Ursache herauszufinden, um eine zielgerechte Therapie finden zu können.PD Dr. Riml kann Ihnen ein breites Spektrum an Operationen anbieten, die Ihre Beschwerden lindern oder gar heilen können. Dazu gehören Operationen an Gelenken (Arthroskopie, Arthroplastik, Gelenksersatz mit Swanson-Prothesen, Gelenksversteifungen, Ganglion-Entfernungen, Denervierung), Bändern und Knochen (Verkürzungsosteotomien, Umstellungsosteotomien, Proximal-row-Carpektomie, Four-Corner-Fusion).</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item border-0">
                                    <h2 class="accordion-header" id="headingSix">
                                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSix">Wiederherstellung</button>
                                    </h2>
                                    <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#myAccordion4">
                                        <div class="card-body">
                                            <p>Korrektureingriffen viele Funktionen der Hand verbessert oder wiederhergestellt werden.Auch tritt der weiße Hautkrebs an der Hand häufig auf. Hier ist es neben der vollständigen Entfernung des Tumors besonders wichtig, die Funktion der Hand nicht zu beeinträchtigen. Deshalb sind hier häufig Gewebeverschiebungen zur Rekonstruktion der Hand nötig. Die Operation kann in örtlicher Betäubung oder Betäubung des gesamten Armes durchgeführt werden.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contact" class="section-contact">
    <div class="container d-flex align-items-center justify-content-center text-center mt-5">
        <div class="text-white mt-5">
            <h1 class="mt-5 fs-3 text-uppercase">
                Sichern Sie Sich Ihren Personlichen Beratungstermin
            </h1>
            <h5 class="mt-3 mb-4">Unverbindlich.Oder nehmen Sie an Einem unserer informationsveranstaltungen teil</h5>
            <a class="btn btn-outline-light btn-lg m-3 text-uppercase" href="javascript:void(0)" role="button" rel="nofollow">
                KONTAKT
            </a>
        </div>
    </div>
</section>

<section class="text-lg-start footer">
    <div class="row p-5">
        <div class="col-lg-3 mt-5">
            <img src="<?= get_template_directory_uri() . '/images/layout/'; ?>logo-r.png" class="rounded mx-auto d-block" alt="...">
        </div>
        <div class="col-lg-3 mt-5">
            <h5 class="text-white fs-4">GESCHÄFTSBEREICHE</h5>
            <p class="text-white fs-6">Plastische Chirurgie</p>
            <p class="text-white fs-6">Riml Cosmetics</p>
            <p class="text-white fs-6">Beauty Apartments</p>
        </div>
        <div class="col-lg-3 mt-5 ">
            <h5 class="text-white fs-4">ÜBER UNS</h5>
            <p class="text-white fs-6">Team & Partner</p>
            <p class="text-white fs-6">Newsfeed</p>
            <p class="text-white fs-6">Anfahrt & Kontakt</p>
            <p class="text-white fs-6">Buchungssystem</p>
        </div>
        <div class="col-lg-3 mt-5 ">
            <h5 class="text-white fs-4">PRAXIS</h5>
            <p class="text-white fs-6">Straße Hausnummer</p>
            <p class="text-white fs-6">Postleitzahl Ort</p>
            <br />
            <p class="text-white fs-6"><strong>Montag-Donnerstag:</strong> 09:00 - 14:00</p>
            <p class="text-white fs-6"><strong>Freitag:</strong> 09:00 - 11:00</p>
        </div>
    </div>
    <div class="border border-top-4 border-white">
        <div class="row p-2">
            <div class="col-4 col-sm-4 text-white text-center">
                @ Copyright 2022 Riml Aesthetics. All Rights Reserved.
            </div>
            <div class="col-7 col-sm-7 text-end">
                <a href="" class="p-2 text-white">Impressum</a>
                <a href="" class="p-2 text-white"> AGB</a>
                <a href="" class="text-white"> Datenschutz</a>
            </div>
            <div class="col-1 col-sm-1">
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>