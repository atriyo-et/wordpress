$(document).ready(function() {
    $('.collapse.in').each(function() {
        $(this).parent().find(".fa").removeClass("fa-plus-square").addClass("fa-minus-square");
    });

    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-plus-square").removeClass("fa-plus-square").addClass("fa-minus-square");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-minus-square").removeClass("fa-minus-square").addClass("fa-plus-square");
    });

    $('.btn-spectrum').on('click', function(event) {       
        if(event.currentTarget.ariaExpanded=="true"){
            $(".line").addClass("p-border");
        }
        else{
            $(".line").removeClass("p-border");
        }
        $(this).siblings().removeClass('active').end().toggleClass('active');
        $('.btn-spectrum').each(function() {
            $(this).parent().find('.btn-spectrum-active').removeClass('btn-spectrum-active');
        });
        $('.multi-collapse').removeClass('show');
        $(event.target).addClass('btn-spectrum-active');
    });

    $('.scroll-down').on('click',function(){
        document.getElementById("introduction").scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    });
});