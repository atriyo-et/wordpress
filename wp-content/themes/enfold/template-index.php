<?php
/*
Template Name: Opening page
*/
?>

<?php get_header(); ?>

<section id="banner" class="section-banner">
    <div class="mask">
        <div class="container d-flex align-items-center justify-content-center text-center">
            <div class="text-white mt-lg-5">
                <h1 class="text-uppercase">Form & Funktion.</h1>
                <h1 class="text-uppercase"> In Perfektion.</h1>
                <h5 class="mb-lg-4">Facharzt für plastische, ästhetische und rekonstruktive Chriurgie</h5>
                <a class="btn btn-outline-light btn-lg m-2 text-uppercase" href="javascript:void(0)" role="button" rel="nofollow">
                    Jetzt Buchen
                </a>
            </div>
        </div>
    </div>
    <div class="scroll-down fa fa-angle-down fa-2x">

    </div>
</section>

<section id="introduction" class="section-introduction">
    <div class="container col-lg-6 pt-5">
        <div class="row justify-content-center text-center ">
            <h1 class="mt-3 fs-1 text-dark">
                Einleitung Und Philosophie
            </h1>
<?php $wpb_all_query = new WP_Query(['post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=> 3, 'category_name'=>'Einleitung Und Philosophie']);

if ( $wpb_all_query->have_posts() ) : 

    while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

            <p class="p-3">
                <?php the_content();?>
            </p>

    <?php endwhile; ?>

<?php wp_reset_postdata();

else : ?>

    <p class="p-3"><?php _e( 'Sorry, nothings here.' ); ?></p>

<?php endif; ?>

        </div>
        <hr>
    </div>
</section>

<section id="activities" class="section-activities">
    <div class="container col-lg-6 pt-5 mt-5">
        <div class="row justify-content-center text-center ">
            <h1 class="mt-5 fs-1 text-dark">
                3 Geschaftsbereizhe
            </h1>
            <p class="p-3">
            Riml Aesthetics bietet Ihnen auf über 450 Quadratmetern alles rund um Ihre Schönheit und Ihr Wohlbefinden. Nutzen Sie unsere gratis Parkplätze direkt vor der Tür, oder die nahen öffentlichen Verkehrsmittel.
            </p>
        </div>
    </div>
    <div class="container d-flex">
        <div class="row">
            <div class="col-12 col-md-4 p-0">
                <div class="card card-first h-100 text-center p-4">
                    <div class="card-body">
                   <div><img src="<?=get_template_directory_uri().'/images/layout/';?>logo-r.png" class="mx-auto d-block" alt="..."></div>
                        <h5 class="card-title mt-5 fs-3">Plastische Chirurgie</h5>
                        <p class="card-text mt-3 mb-3 "> Plastische Chirurgie und Medizinische Kosmetik bilden eine perfekte Symbiose. Alles für Ihr Wohlbefinden und Ihre Schönheit finden Sie bei uns unter einem Dach. </p>
                        <a href="#" class="btn btn-outline-light m-2 text-uppercase">MEHR INFO</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 p-0">
                <div class="card card-second h-100 text-center p-4">
                    <div class="card-body">
                    <div><img src="<?=get_template_directory_uri().'/images/layout/';?>logo-r.png" class="mx-auto d-block" alt="..."></div>
                        <h5 class="card-title mt-5 fs-3">Riml Cosmetics</h5>
                        <p class="card-text mt-3 mb-3"> Riml Aesthetics bietet Ihnen auf über 450 Quadratmetern alles rund um Ihre Schönheit und Ihr Wohlbefinden. Nutzen Sie unsere gratis Parkplätze direkt vor der Tür, oder die nahen öffentlichen Verkehrsmittel. </p>
                        <a href="#" class="btn btn-outline-light m-2 text-uppercase">MEHR INFO</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 p-0">
                <div class="card card-third h-100 text-center p-4">
                    <div class="card-body">
                    <div><img src="<?=get_template_directory_uri().'/images/layout/';?>logo-r.png" class="mx-auto d-block" alt="..."></div>
                        <h5 class="card-title mt-5 fs-3">Beauty Apartments</h5>
                        <p class="card-text mt-3 mb-3"> Wenn Sie eine längere Anreise zu uns haben, oder sich nach der Operation oder kosmetischen Behandlung ausruhen möchten, bieten wir Ihnen gern unsere Beauty Apartments an. </p>
                        <a href="#" class="btn btn-outline-light m-2 text-uppercase">MEHR INFO</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="middle-banner" class="section-middle-banner mt-5"></section>

<section id="section-carousel" class="section-carousel pt-5 pb-5">
    <div class="container">
        <div class="row">            
            <div class="col-12 text-right">
                <a class="btn border border-secondary rounded-0 mb-3" data-target="#carouselExampleIndicators2" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="btn border border-secondary rounded-0 mb-3" data-target="#carouselExampleIndicators2" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
            <div class="col-12">
                <div id="carouselExampleIndicators2" class="carousel carousel-dark slide carousel-fade" data-bs-interval="false">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="<?=get_template_directory_uri().'/images/layout/';?>Plastische-Chirurgie-Dr-Riml-Gesicht-1-klein.jpg">
                                        <div class="card-body">
                                            <h4 class="card-title">Ordination normal geöffnet</h4>
                                            <h5 class="text-muted">21. November 2021</h5>
                                            <p class="card-text">Lorem ipsum blah blah blah jksdhfj shfjkh sdjkhfsjd hfjklshfjk lshdjkflh </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="<?=get_template_directory_uri().'/images/layout/';?>Plastische-Chirurgie-Dr-Riml-Hand-1-klein.jpg">
                                        <div class="card-body">
                                            <h4 class="card-title">Karpaltunnelsyndrom</h4>
                                            <h5 class="text-muted">30. August 2021</h5>
                                            <p class="card-text">Lorem ipsum blah blah blah jksdhfj shfjkh sdjkhfsjd hfjklshfjk lshdjkflh</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="<?=get_template_directory_uri().'/images/layout/';?>Plastische-Chirurgie-Dr-Riml-Gesicht-inter1.jpg">
                                        <div class="card-body">
                                            <h4 class="card-title">Lidstraffung</h4>
                                            <h5 class="text-muted">28. August 2021</h5>
                                            <p class="card-text">Lorem ipsum blah blah blah jksdhfj shfjkh sdjkhfsjd hfjklshfjk lshdjkflh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="https://images.unsplash.com/photo-1532771098148-525cefe10c23?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=1080&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjMyMDc0fQ&amp;s=3f317c1f7a16116dec454fbc267dd8e4">
                                        <div class="card-body">
                                            <h4 class="card-title">Special title treatment</h4>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="https://images.unsplash.com/photo-1532715088550-62f09305f765?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=1080&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjMyMDc0fQ&amp;s=ebadb044b374504ef8e81bdec4d0e840">
                                        <div class="card-body">
                                            <h4 class="card-title">Special title treatment</h4>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="https://images.unsplash.com/photo-1506197603052-3cc9c3a201bd?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=1080&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjMyMDc0fQ&amp;s=0754ab085804ae8a3b562548e6b4aa2e">
                                        <div class="card-body">
                                            <h4 class="card-title">Special title treatment</h4>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=1080&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjMyMDc0fQ&amp;s=ee8417f0ea2a50d53a12665820b54e23">
                                        <div class="card-body">
                                            <h4 class="card-title">Special title treatment</h4>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="https://images.unsplash.com/photo-1532777946373-b6783242f211?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=1080&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjMyMDc0fQ&amp;s=8ac55cf3a68785643998730839663129">
                                        <div class="card-body">
                                            <h4 class="card-title">Special title treatment</h4>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="https://images.unsplash.com/photo-1532763303805-529d595877c5?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=1080&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjMyMDc0fQ&amp;s=5ee4fd5d19b40f93eadb21871757eda6">
                                        <div class="card-body">
                                            <h4 class="card-title">Special title treatment</h4>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contact" class="section-contact">
    <div class="container d-flex align-items-center justify-content-center text-center">
        <div class="text-white mt-5">
            <h1 class="mt-5 fs-3 text-uppercase">
                Sichern Sie Sich Ihren Personlichen Beratungstermin
            </h1>
            <h5 class="mt-3 mb-4">Unverbindlich.Oder nehmen Sie an Einem unserer informationsveranstaltungen teil</h5>
            <a class="btn btn-outline-light btn-lg m-3 text-uppercase" href="javascript:void(0)" role="button" rel="nofollow">
                KONTAKT
            </a>
        </div>
    </div>
</section>

<section class="text-lg-start footer">
    <div class="row p-5">
        <div class="col-lg-3 mt-5">
            <img src="<?=get_template_directory_uri().'/images/layout/';?>logo-r.png" class="rounded mx-auto d-block" alt="...">
        </div>
        <div class="col-lg-3 mt-5">
            <h5 class="text-white fs-4">GESCHÄFTSBEREICHE</h5>
            <p class="text-white fs-6">Plastische Chirurgie</p>
            <p class="text-white fs-6">Riml Cosmetics</p>
            <p class="text-white fs-6">Beauty Apartments</p>
        </div>
        <div class="col-lg-3 mt-5 ">
            <h5 class="text-white fs-4">ÜBER UNS</h5>
            <p class="text-white fs-6">Team & Partner</p>
            <p class="text-white fs-6">Newsfeed</p>
            <p class="text-white fs-6">Anfahrt & Kontakt</p>
            <p class="text-white fs-6">Buchungssystem</p>
        </div>
        <div class="col-lg-3 mt-5 ">
            <h5 class="text-white fs-4">PRAXIS</h5>
            <p class="text-white fs-6">Straße Hausnummer</p>
            <p class="text-white fs-6">Postleitzahl Ort</p>
            <br/>
            <p class="text-white fs-6"><strong>Montag-Donnerstag:</strong> 09:00 - 14:00</p>
            <p class="text-white fs-6"><strong>Freitag:</strong> 09:00 - 11:00</p>
        </div>
    </div>
    <div class="border border-top-4 border-white">
        <div class="row p-2">
            <div class="col-4 col-sm-4 text-white text-center">
                @ Copyright 2022 Riml Aesthetics. All Rights Reserved.
            </div>
            <div class="col-7 col-sm-7 text-end">
                <a href="" class="p-2 text-white">Impressum</a>
                <a href="" class="p-2 text-white"> AGB</a>
                <a href="" class="text-white"> Datenschutz</a>
            </div>
            <div class="col-1 col-sm-1">
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>