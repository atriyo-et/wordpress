<!DOCTYPE html>
<html>
    <head>
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?> >  
<header class="sticky-top">
    
    <?php wp_nav_menu([
        'theme_location' => 'header-menu',
        'menu_class' => 'navbar'
    ]); ?>

</header>      