<?php

function loadStylesheets()
{
    wp_register_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', [], false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri().'/style.css', [], false, 'all');
    wp_enqueue_style('style');
}
add_action('wp_enqueue_scripts', 'loadStylesheets');

function loadJquery()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri().'/js/jquery-3.6.0.min.js', [], false, true);
    wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'loadJquery');

function loadScripts()
{
    wp_register_script('bootstrap', get_template_directory_uri().'/js/bootstrap.js', [], false, true);
    wp_enqueue_script('bootstrap');
}
add_action('wp_enqueue_scripts', 'loadScripts');

add_theme_support('menus');
add_theme_support('post-thumbnails');

register_nav_menus([
    'header-menu' => __('Header Menu', 'theme'),
    'footer-menu' => __('Footer Menu', 'theme')
]);

add_image_size('smallest', 300, 300, true);
add_image_size('largest', 800, 800, true);
?>